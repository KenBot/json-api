package kenbot.web.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import kaendfinger.kenbot.core.KenBotConfig;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class Config implements HttpHandler {
    @Override
    public void handle(HttpExchange t) throws IOException {
        Map<String, Object> params = (Map<String, Object>) t.getAttribute("parameters");
        GsonBuilder builder = new GsonBuilder();
        if (params.containsKey("pretty")) {
            if (!params.get("pretty").equals("false")) {
                builder.setPrettyPrinting();
            }
        }
        Gson gson = builder.create();
        BufferedWriter out = new BufferedWriter(new PrintWriter(t.getResponseBody()));
        if (!params.containsKey("key")) {
            out.write(gson.toJson("No Key Specified"));
            out.flush();
            out.close();
            return;
        }
        String key = (String) params.get("key");
        String value = KenBotConfig.get(key);
        ConfigJson json = new ConfigJson(key, value);
        String response = gson.toJson(json);
        t.sendResponseHeaders(200, response.length());
        t.getResponseBody().write(response.getBytes());
        out.flush();
        out.close();
    }
}