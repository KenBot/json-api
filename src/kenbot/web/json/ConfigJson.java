package kenbot.web.json;

public class ConfigJson {
    private String key;
    private String value;

    public ConfigJson(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
