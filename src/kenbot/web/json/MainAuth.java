package kenbot.web.json;

import com.sun.net.httpserver.BasicAuthenticator;

public class MainAuth extends BasicAuthenticator {
    public MainAuth(String realm) {
        super(realm);
    }

    @Override
    public boolean checkCredentials(String username, String password) {
        return ModuleJSONApi.usernames.contains(username) && ModuleJSONApi.passwords.contains(password);
    }
}
