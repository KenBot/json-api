package kenbot.web.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.core.KenBotConfig;
import kaendfinger.kenbot.listeners.CommandListener;
import org.pircbotx.hooks.events.MessageEvent;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class RunCommand implements HttpHandler {
    @Override
    public void handle(HttpExchange t) throws IOException {
        Map<String, Object> params = (Map<String, Object>) t.getAttribute("parameters");
        GsonBuilder builder = new GsonBuilder();
        if (params.containsKey("pretty")) {
            if (!params.get("pretty").equals("false")) {
                builder.setPrettyPrinting();
            }
        }
        Gson gson = builder.create();
        OutputStream out = t.getResponseBody();
        if (!params.containsKey("command") || !params.containsKey("channel")) {
            byte[] response = gson.toJson("No Command/Channel Specified").getBytes();
            t.sendResponseHeaders(200, response.length);
            out.write(response);
            out.close();
            return;
        }
        String command = (String) params.get("command");
        String channel = (String) params.get("channel");
        String args = (String) params.get("args");
        if (args==null) {
            args = "";
        } else {
            args = " " + args;
        }
        boolean didRun = false;
        if (CommandListener.commandExists(command) || BotUtils.getBot().getChannelsNames().contains(channel)) {
            String prefix = KenBotConfig.get("prefix");
            Method method = CommandListener.getCommandMethod(command);
            try {
                if (method.getParameterTypes().length>1) {
                    method.invoke(null, new MessageEvent<>(BotUtils.getBot(), BotUtils.getBot().getChannel(channel), BotUtils.getBot().getUserBot(), prefix + command + args), args.substring(1));
                } else {
                    method.invoke(null, new MessageEvent<>(BotUtils.getBot(), BotUtils.getBot().getChannel(channel), BotUtils.getBot().getUserBot(), prefix + command + args));
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
            didRun = true;
        }
        byte[] response = gson.toJson(new Response(didRun)).getBytes();
        t.sendResponseHeaders(200, response.length);
        out.write(response);
        out.flush();
        out.close();
    }

    class Response {
        private boolean didRun;

        public Response(boolean didRun) {
            this.didRun = didRun;
        }
    }
}