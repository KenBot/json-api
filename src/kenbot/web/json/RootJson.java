package kenbot.web.json;

import java.util.Date;
import java.util.List;

public class RootJson {
    private long timestamp;
    private configuration configuration;
    private List<String> channels;

    public RootJson(List<String> channels, String server, String user, String prefix, String realName) {
        this.configuration = new configuration(server, user, prefix, realName);
        this.channels = channels;
        this.timestamp = new Date().getTime();
    }

    class configuration {
        private String server;
        private String user;
        private String prefix;
        private String realName;

        public configuration(String server, String user, String prefix, String realName) {
            this.server = server;
            this.user = user;
            this.prefix = prefix;
            this.realName = realName;
        }
    }
}
