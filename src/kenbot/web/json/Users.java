package kenbot.web.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import kaendfinger.kenbot.api.utils.BotUtils;
import org.pircbotx.User;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public class Users implements HttpHandler {

    @Override
    public void handle(HttpExchange t) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Map<String, Object> params = (Map<String, Object>) t.getAttribute("parameters");
        OutputStream stream = t.getResponseBody();
        Set<String> channels = BotUtils.getBot().getChannelsNames();
        HashMap<String, List> users = new HashMap<>();
        if (params.containsKey("channel")) {
            String channel = (String) params.get("channel");
            ArrayList<String> list = new ArrayList<>();
            if (!BotUtils.getBot().getChannelsNames().contains(channel)) {
                byte[] response = gson.toJson(new Error("error_not_in_channel")).getBytes();
                t.sendResponseHeaders(200, response.length);
                stream.write(response);
                stream.flush();
                stream.close();
                return;
            }
            for (User user : BotUtils.getBot().getChannel(channel).getUsers()) {
                list.add(user.getNick());
            }
            users.put(channel, list);
            byte[] response = gson.toJson(users).getBytes();
            t.sendResponseHeaders(200, response.length);
            stream.write(response);
            stream.close();
            return;
        }
        for (String channel : channels) {
            ArrayList<String> list = new ArrayList<>();
            for (User user : BotUtils.getBot().getChannel(channel).getUsers()) {
                list.add(user.getNick());
            }
            users.put(channel, list);
        }
        byte[] response = gson.toJson(users).getBytes();
        t.sendResponseHeaders(200, response.length);
        stream.write(response);
        stream.close();
    }

    class Error {
        private String error;

        public Error(String error) {
            this.error = error;
        }
    }
}
