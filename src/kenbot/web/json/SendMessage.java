package kenbot.web.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import kaendfinger.kenbot.api.utils.BotUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class SendMessage implements HttpHandler {
    @Override
    public void handle(HttpExchange t) throws IOException {
        Gson gson = new GsonBuilder().
                setPrettyPrinting().
                serializeNulls().
                create();
        Map<String, Object> params = (Map<String, Object>) t.getAttribute("parameters");
        Object json;
        if (!params.containsKey("target") || !params.containsKey("message")) {
            json = "ERROR! Target or Message was not specified!";
        } else {
            boolean sent;
            String target = (String) params.get("target");
            String message = (String) params.get("message");
            if (!BotUtils.getBot().isConnected()) {
                sent = false;
            } else {
                BotUtils.getBot().sendMessage(target, message);
                sent = true;
            }
            json = new SendMessageJson(sent);
        }
        byte[] response = gson.toJson(json).getBytes();
        t.sendResponseHeaders(200, response.length);
        OutputStream os = t.getResponseBody();
        os.write(response);
        os.close();
    }
}