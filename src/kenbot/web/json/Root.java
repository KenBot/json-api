package kenbot.web.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.core.KenBotConfig;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public class Root implements HttpHandler {
    @Override
    public void handle(HttpExchange t) throws IOException {
        Gson gson = new GsonBuilder().
                setPrettyPrinting().
                serializeNulls().
                create();
        String server = BotUtils.getBot().getServer();
        String user = BotUtils.getBot().getNick();
        String prefix = KenBotConfig.get("prefix");
        String realName = BotUtils.getBot().getVersion();
        Set<String> channelsNames = BotUtils.getBot().getChannelsNames();
        List<String> channels = Arrays.asList(channelsNames.toArray(new String[channelsNames.size()]));
        RootJson json = new RootJson(channels, server, user, prefix, realName);
        byte[] response = gson.toJson(json).getBytes();
        t.sendResponseHeaders(200, response.length);
        OutputStream os = t.getResponseBody();
        os.write(response);
        os.close();
    }
}
