package kenbot.web.json;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import kaendfinger.kenbot.api.IModule;
import kaendfinger.kenbot.api.events.ConfigurationEvent;
import kaendfinger.kenbot.api.utils.StringUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;

public class ModuleJSONApi implements IModule {
    private static HttpServer server;
    private int port;
    protected static List<String> usernames;
    protected static List<String> passwords;

    @Override
    public void setup(ConfigurationEvent configurationEvent) {
        configurationEvent.getConfiguration().header("Port to use for Configuration");
        port = Integer.parseInt(configurationEvent.getConfiguration().get("port", "7000"));
        usernames = Arrays.asList(StringUtils.getQuoted(configurationEvent.getConfiguration().get("usernames", "kaendfinger")));
        passwords = Arrays.asList(StringUtils.getQuoted(configurationEvent.getConfiguration().get("passwords", "kaendfinger")));
    }

    @Override
    public void load() {
        try {
            server = HttpServer.create(new InetSocketAddress(port), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.createContext("/", new Root());
        HttpContext run = server.createContext("/run", new RunCommand());
        run.getFilters().add(new ParameterFilter());
        run.setAuthenticator(new MainAuth("BotAPI:RunCommand"));
        server.createContext("/users", new Users()).getFilters().add(new ParameterFilter());
        HttpContext send = server.createContext("/sendMessage", new SendMessage());
        send.getFilters().add(new ParameterFilter());
        send.setAuthenticator(new MainAuth("BotAPI:SendMessage"));
        HttpContext config = server.createContext("/config", new Config());
        config.getFilters().add(new ParameterFilter());
        config.setAuthenticator(new MainAuth("BotAPI:Config"));
        server.setExecutor(null);
        System.out.println("Starting JSON Api server on port " + port);
        server.start();
    }

    @Override
    public void unload() {
        server.stop(0);
    }

    @Override
    public String[] classes() {
        return new String[0];
    }

    @Override
    public String name() {
        return "JSONApi";
    }
}
